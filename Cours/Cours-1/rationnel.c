// rationnel.c

#include <stdio.h>
#include "rationnel.h"

// Constructeur. Initialise A à p/q (q supposé non nul)
void init_rationnel (struct rationnel* A, int p, int q)
{
    A->numer = p;
    A->denom = q;
}

// Affecte à A le rationnel B + C
// A mode R. B et C mode D.

// Remarque : ce code fonctionne mais est faux sur le principe

void add_rationnel (struct rationnel* A, struct rationnel* B, struct rationnel* C)
{   int p, q;
    p = B->numer*C->denom + B->denom*C->numer;
    q = B->denom*C->denom;
    init_rationnel (A, p, q);
}


// Destructeur
void clear_rationnel (struct rationnel* A)
{
}


// Affiche le rationnel sur la sortie standard
void print_rationnel (struct rationnel* A)
{
    printf ("(%d)/(%d)\n", A->numer, A->denom);
}


