// rationnel.h

// 1. Déclaration du type

struct rationnel {
    int numer;
    int denom;
};

/*
 * 2. Spécifications
 * Le type struct rationnel implante des nombres rationnels
 * Le champ denom est non nul.
 * La fraction n'est pas nécessairement réduite.
 * Les deux champs peuvent être de signe quelconque.
 */

// 3. Les prototypes

// Constructeur. Initialise A à p/q (q supposé non nul)
extern void init_rationnel (struct rationnel* A, int p, int q);

// Affecte à A le rationnel B + C
// A mode R. B et C mode D.
extern void add_rationnel (struct rationnel* A, struct rationnel* B, struct rationnel* C);

// Destructeur
extern void clear_rationnel (struct rationnel*);

// Affiche le rationnel sur la sortie standard
extern void print_rationnel (struct rationnel*);

