// main.c

#include "rationnel.h"

int main ()
{
    struct rationnel A, B;

    init_rationnel (&A, 3, 4); // A = 3/4
    init_rationnel (&B, -5, 7); // B = -5/7
    add_rationnel (&A, &A, &B); // A = A + B
    print_rationnel (&A);       // on affiche le résultat
    clear_rationnel (&A);       // on appelle les destructeurs
    clear_rationnel (&B);
    return 0;
}

