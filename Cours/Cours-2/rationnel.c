// rationnel.c

#include <stdio.h>
#include <stdlib.h>
#include "rationnel.h"

static void set_rationnel (struct rationnel* A, int p, int q)
{
    A->data[0] = p;
    A->data[1] = q;
}

// Constructeur. Initialise A à p/q (q supposé non nul)
void init_rationnel (struct rationnel* A, int p, int q)
{
    A->data = (int*)malloc (2 * sizeof(int));
    set_rationnel (A, p, q);
}

// Affecte à A le rationnel B + C
// A mode R. B et C mode D.

void add_rationnel (struct rationnel* A, struct rationnel* B, struct rationnel* C)
{   int p, q;
    p = B->data[0]*C->data[1] + B->data[1]*C->data[0];
    q = B->data[1]*C->data[1];
    set_rationnel (A, p, q);
}


// Destructeur
void clear_rationnel (struct rationnel* A)
{
  free (A->data);
}


// Affiche le rationnel sur la sortie standard
void print_rationnel (struct rationnel* A)
{
    printf ("(%d)/(%d)\n", A->data[0], A->data[1]);
}


