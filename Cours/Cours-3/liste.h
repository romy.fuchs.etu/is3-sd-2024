// liste.h

// Les maillons sont alloués dans le tas (dynamiquement)
struct maillon {
    double valeur;
    struct maillon * suivant;   // adresse du maillon suivant de la liste
};

// NIL = marqueur de fin (champ suivant du dernier maillon de la liste)
//
#define NIL (struct maillon *)0

struct liste {
    struct maillon * tete;  // adresse du premier maillon
    int nbelem;             // nombre de maillons présents dans la liste
};

// liste en mode R
extern void init_liste (struct liste*);

// Ajoute d (mode D) en tête de la liste L (mode DR)
extern void ajout_en_tete_liste (struct liste* L, double d);

// Affiche la liste (mode D)
extern void print_liste (struct liste);

// Destructeur (mode ??? Disons DR)
extern void clear_liste (struct liste*);

