// main.c - exemple de programme qui utilise des listes

#include "liste.h"

int main ()
{
    struct liste L0;

    init_liste (&L0);   // L0 initialisée à la liste vide
    ajout_en_tete_liste (&L0, 2024);    // 1 malloc
    ajout_en_tete_liste (&L0, 2);       // 1 malloc
    ajout_en_tete_liste (&L0, 6);       // 1 mallc
    print_liste (L0);   // affiche [6, 2, 2024]
                        // pas de malloc : on parcourt les maillons existants
    clear_liste (&L0);  // destructeur - 3 free
    return 0;
}
