// liste.c

#include <stdio.h>
#include <stdlib.h>
#include "liste.h"

// liste en mode R
void init_liste (struct liste* L)
{
  L->tete = NIL;
  L->nbelem = 0;
}

// Ajoute d (mode D) en tête de la liste L (mode DR)
void ajout_en_tete_liste (struct liste* L, double d)
{
  struct maillon * M;
// 1
  M = (struct maillon *)malloc (sizeof (struct maillon));
// 2
  M->valeur = d;
// 3
  M->suivant = L->tete;
// 4
  L->tete = M;
// 5
  L->nbelem += 1;
}

// Affiche la liste (mode D)
void print_liste (struct liste L)
{
  struct maillon * M;
  int i;

  M = L.tete;
  printf ("[");
  for (i = 0; i < L.nbelem; i++)
    {
      if (i > 0)
        printf (", ");
      printf ("%lf", M->valeur);
// nouvelle valeur de M = le contenu champ suivant dela structure dont 
//                        l'adresse est dans l'ancienne valeur de M
      M = M->suivant;
    }
    printf ("]\n");
}

// Destructeur (mode ??? Disons DR)
void clear_liste (struct liste* L)
{
}

