// main.c

#include <stdio.h>
#include "file.h"

int main ()
{
  struct file F;
  double data [] = { 128, 224, 14, -5, 8, 45, -18 };
  int N = sizeof(data)/sizeof(double);
  int i;

  init_file (&F);   // F est initialisée à vide
  for (i = 0; i < N; i++)
    {
      printf ("on enfile : %lf\n", data[i]);
      enfiler (&F, data[i]);
    }
  while (! est_vide_file (&F))
    {
      double d;
      defiler (&d, &F);
      printf ("on vient de défiler : %lf\n", d);
    }
  clear_file (&F);
  return 0;
}
