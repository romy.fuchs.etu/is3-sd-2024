// file.c

#include <assert.h>
#include "file.h"

void init_file (struct file* F)
{
  F->we = 0;
  F->re = 1;
  assert (est_vide_file (F));
}

void enfiler (struct file* F, double d)
{
  F->we = (F->we + 1) % NBMAX;
  F->T [F->we] = d;
  assert (! est_vide_file (F));
}

bool est_vide_file (struct file* F)
{
  return (F->we + 1) % NBMAX == F->re;
}

void defiler (double* d, struct file* F)
{
  assert (! est_vide_file (F));
  *d = F->T [F->re];
  F->re = (F->re + 1) % NBMAX;
}

void clear_file (struct file* F)
{
}

