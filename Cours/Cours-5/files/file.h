// file.h

#define NBMAX 7

struct file {
    double T [NBMAX];
    int re;
    int we;
};

/*
 * re = l'extrémité en lecture = indice du prochain élément défilé
 * we = l'extrémité en écriture = indice du dernier élément enfilé
 * la file est vide quand (we + 1) % NBMAX == re
 * la file a une capacité de NBMAX-1 éléments
 */

// File en mode R
extern void init_file (struct file*);

// File en mode DR et double en mode D
extern void enfiler (struct file*, double);

#include <stdbool.h>

// File en mode D
extern bool est_vide_file (struct file*);

// le double en mode R et la file en mode DR
extern void defiler (double*, struct file*);

// File en mode R (????)
extern void clear_file (struct file*);

