// pile.c

#include <assert.h>
#include "pile.h"

void init_pile (struct pile* P)
{
  P->sp = 0;
}

void empiler (struct pile* P, double d)
{
  assert (P->sp < NBMAX);
  P->T[P->sp] = d;
  P->sp += 1;
// ou bien
//  P->T[P->sp++] = d;
}

bool est_vide_pile (struct pile* P)
{
  return P->sp == 0;
}

void depiler (double* d, struct pile* P)
{
  assert (! est_vide_pile (P));
  P->sp -= 1;
  *d = P->T[P->sp];
// ou bien
//  *d = P->T[--P->sp];
}

void clear_pile (struct pile* P)
{
}

