// main.c

#include <stdio.h>
#include "pile.h"

int main ()
{
  struct pile P;
  double data [] = { 128, 224, 14, -5, 8 };
  int N = sizeof(data)/sizeof(double);
  int i;

  init_pile (&P);   // P est initialisée à vide
  for (i = 0; i < N; i++)
    {
      printf ("on empile : %lf\n", data[i]);
      empiler (&P, data[i]);
    }
  while (! est_vide_pile (&P))
    {
      double d;
      depiler (&d, &P);
      printf ("on vient de dépiler : %lf\n", d);
    }
  clear_pile (&P);
  return 0;
}
