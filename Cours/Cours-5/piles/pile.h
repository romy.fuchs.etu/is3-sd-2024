// pile.h

#define NBMAX 4

struct pile {
    double T [NBMAX];
    int sp;
};

/*
 * Les éléments sont empilés à partir de l'indice 0
 * sp = l'indice de la première case libre
 */

// Pile en mode R
extern void init_pile (struct pile*);

// Pile en mode DR et double en mode D
extern void empiler (struct pile*, double);

#include <stdbool.h>

// Pile en mode D
extern bool est_vide_pile (struct pile*);

// le double en mode R et la pile en mode DR
extern void depiler (double*, struct pile*);

// Pile en mode R (????)
extern void clear_pile (struct pile*);

