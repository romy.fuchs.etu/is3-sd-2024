// main.c

#include <stdio.h>
#include "abr.h"

int main ()
{
  double data [] = { 17, 23, 109, 5, 14, 31 };
  int n = sizeof(data)/sizeof(data[0]);

  struct noeud* A;

  A = NIL;
  for (int i = 0; i < n; i++)
    {
      ajouter_abr (&A, data[i]);
    }
  if (rechercher_abr (23, A))
    printf ("23 est présent dans A\n");
  else
    printf ("23 n'est pas présent dans A\n");
  if (rechercher_abr (24, A))
    printf ("24 est présent dans A\n");
  else
    printf ("24 n'est pas présent dans A\n");
  clear_abr (A);
  return 0;
}

