// abr.c

#include <stdlib.h>
#include "abr.h"

static 
        struct noeud* 
                        new_feuille (double d)
{
  struct noeud *F;
  F = (struct noeud*)malloc (sizeof (struct noeud));
  F->gauche = NIL;
  F->valeur = d;
  F->droit = NIL;
  return F;
}

/*
 * A0 pointeur passé en mode DR
 * Le seul cas où *A0 est modifié, c'est le cas où l'arbre est vide
 */

void ajouter_abr (struct noeud** A0, double d)
{
  struct noeud *A = *A0;

  if (A == NIL)
    *A0 = new_feuille (d);
  else
    {
      struct noeud* B = NIL;
/*
 * A = le noeud courant
 * B = la précédente valeur de A (initialisée à NIL pour éviter un warning)
 */
      while (A != NIL)
        {
          B = A;
          if (A->valeur < d)
            A = A->droit;
          else
            A = A->gauche;
        }
/*
 * A vaut NIL - on ajoute une feuille à B
 */
      if (B->valeur < d)
        B->droit = new_feuille (d);
      else
        B->gauche = new_feuille (d);
    }
}

bool rechercher_abr (double d, struct noeud* A0)
{
  bool found = false;
  struct noeud* A;

  A = A0;
  while (A != NIL && !found)
    {
      if (A->valeur == d)
        found = true;
      else if (A->valeur < d)
        A = A->droit;
      else
        A = A->gauche;
    }
    return found;
}

void clear_abr (struct noeud* A0)
{
}

