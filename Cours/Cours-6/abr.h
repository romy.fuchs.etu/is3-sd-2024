// abr.h

#include <stdbool.h>

struct noeud {
    struct noeud* gauche;   // toutes les clés < valeur
    double  valeur;         // ici valeur = clé
    struct noeud* droit;    // toutes les clés > valeur
};

#define NIL (struct noeud*)0

// ABR en mode DR et double en mode D
extern void ajouter_abr (struct noeud**, double);

// double et ABR passés en mode D
extern bool rechercher_abr (double, struct noeud*);

// destructeur
extern void clear_abr (struct noeud*);

