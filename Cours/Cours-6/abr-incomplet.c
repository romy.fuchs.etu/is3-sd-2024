// abr.c

#include <stdlib.h>
#include "abr.h"

static 
        struct noeud* 
                        new_feuille (double d)
{
  struct noeud *F;
  F = (struct noeud*)malloc (sizeof (struct noeud));
  F->gauche = NIL;
  F->valeur = d;
  F->droit = NIL;
  return F;
}

void ajouter_abr (struct noeud** A0, double d)
{
  struct noeud *A = *A0;
  if (A == NIL)
    *A0 = new_feuille (d);
  else
    {
      
    }
}
bool rechercher_abr (double d, struct noeud* A0)
{
  bool found = false;
  struct noeud* A;

  A = A0;
  while (A != NIL && !found)
    {
      if (A->valeur == d)
        found = true;
      else if (A->valeur < d)
        A = A->droit;
      else
        A = A->gauche;
    }
    return found;
}

void clear_abr (struct noeud* A0)
{
}

