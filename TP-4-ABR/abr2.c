#include <stdio.h>
#include <stdlib.h>
#include "abr.h"

static struct noeud * new_feuille(double d){
    struct noeud *F;
    F=(struct noeud *)malloc (sizeof(struct noeud));
    F->gauche=NIL;
    F->valeur=d;
    F->droit=NIL;
    return F;
}

struct noeud * ajouter_abr(struct noeud * A0 , double d){
    struct noeud * A=A0;
    if (A==NIL)
        A0=new_feuille(d);
    else
        struct noeud * B=NIL;
    while(A!=NIL){
        B=A;
        if(A->valeur<d)
            A=A->droit;
        else
            A=A->gauche;
    }
    if(B->valeur<d){
        B->droit=new_feuille(d);
    else
        B->gauche=new_feuille(d);
    }
    return A0;
}
