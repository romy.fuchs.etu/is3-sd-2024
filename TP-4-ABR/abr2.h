#include <stdbool.h>

struct noeud{
    struct noeud * gauche;
    double valeur;
    struct noeud * droit;
};
#define NIL(struct noeud *)0

extern struct noeud * ajouter_abr(struct noeud *, double);

extern void afficher_abr(struct noeud *);

extern int hauteur_abr(struct noeud *);

extern int nombre_noeuds_abr(struct noeud *);

extern bool rechercher_abr(double, struct noeud *);

extern void clear_abr(struct noeud *);
