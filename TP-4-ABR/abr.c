// abr.c
#include <stdio.h>
#include <stdlib.h>
#include "abr.h"

static
        struct noeud*
                        new_feuille (double d)
{
  struct noeud *F;
  F = (struct noeud*)malloc (sizeof (struct noeud));
  F->gauche = NIL;
  F->valeur = d;
  F->droit = NIL;
  return F;
}

/*
 * A0 pointeur passé en mode DR
 * Le seul cas où *A0 est modifié, c'est le cas où l'arbre est vide
 */

struct noeud * ajouter_abr (struct noeud* A0, double d)
{
  struct noeud *A = A0;

  if (A == NIL)
    A0 = new_feuille (d);
  else
    {
      struct noeud* B = NIL;
/*
 * A = le noeud courant
 * B = la précédente valeur de A (initialisée à NIL pour éviter un warning)
 */
      while (A != NIL)
        {
          B = A;
          if (A->valeur < d)
            A = A->droit;
          else
            A = A->gauche;
        }
/*
 * A vaut NIL - on ajoute une feuille à B
 */
      if (B->valeur < d)
        B->droit = new_feuille (d);
      else
        B->gauche = new_feuille (d);
    }
    return A0;
}

bool rechercher_abr (double d, struct noeud* A0)
{
  bool found = false;
  struct noeud* A;

  A = A0;
  while (A != NIL && !found)
    {
      if (A->valeur == d)
        found = true;
      else if (A->valeur < d)
        A = A->droit;
      else
        A = A->gauche;
    }
    return found;
}

void clear_abr (struct noeud* A0)
{
    if(A0!=NIL){
        clear_abr(A0->gauche);
        clear_abr(A0->droit);
        free(A0);
    }
}

void afficher_abr(struct noeud * A0){
    if(A0!=NIL){
        afficher_abr(A0->gauche);
        printf("%lf\n",A0->valeur);
        afficher_abr(A0->droit);
    }
}

// utilise des fonction récursive pour hauteur et nombre de noeud. Tu pars du principe que la fonction marche déjà et donc tu l'utilise pour avoir les valeur à dreoite et à gauche'
int hauteur_abr(struct noeud * A0){
    int h;
    if(A0!=NIL){
        int hg=hauteur_abr(A0->gauche);
        int hd=hauteur_abr(A0->droit);
        if(hg<hd)
            h=hd+1;
        else
            h=hg+1;
    }
    else
        h=-1; //si A0 n'a pas de valeur alors la hauteur vaut -1 (def du cours)

    return h;
}


int nombre_noeud_abr(struct noeud *A0){
    int ng;
    int nd;
    int n;
    if (A0!=NIL){
         nd=nombre_noeud_abr(A0->droit);
         ng=nombre_noeud_abr(A0->gauche);
         n=ng+nd+1;
    }
    else
        n=0; //Si A0 n'a pas de valeur alors le nombre de noeud vaut 0(cf cours)'

    return n;
}
void dot2(FILE *f, struct noeud *A0){
  if(A0->gauche!=NIL){
    fprintf(f,"%lf->%lf[label=\"gauche\"];\n",A0->valeur,A0->gauche->valeur);
    dot2(f,A0->gauche);

  }
  if(A0->droit!=NIL){
    fprintf(f,"%lf->%lf[label=\"droit\"];\n",A0->valeur,A0->droit->valeur);
    dot2(f,A0->droit);
  }
}



void dot_abr(struct noeud * A0){
  FILE *fi;
  fi=fopen("abr.dot","w");
  fprintf(fi,"digraph G{\n");
  if(A0==NIL)
    fprintf(fi,"Nil,\n");
  else if(A0->gauche==NIL && A0->droit==NIL)
    fprintf(fi,"%lf\n",A0->valeur);
  else{
    dot2(fi,A0);
    fprintf(fi,"}\n");
    fclose(fi);
    system("dot -Tpdf abr.dot -Grankdir=LR -o abr.pdf");
    system("evince abr.pdf");
  }

}






