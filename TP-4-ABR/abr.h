#include <stdbool.h>

struct noeud {
    struct noeud* gauche;   // toutes les clés < valeur
    double  valeur;         // ici valeur = clé
    struct noeud* droit;    // toutes les clés > valeur
};

#define NIL (struct noeud*)0

// ABR en mode DR et double en mode D
extern struct noeud * ajouter_abr (struct noeud*, double);

// double et ABR passés en mode D
extern bool rechercher_abr (double, struct noeud*);

// destructeur
extern void clear_abr (struct noeud*);

extern void afficher_abr(struct noeud *);

extern int hauteur_abr(struct noeud *);

extern int nombre_noeud_abr(struct noeud *);

extern void dot_abr(struct noeud *);
