#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(){
    int c;
    int i;
    char * tab;
    tab=(char *) malloc(64 * sizeof(char));
    i=0;
    c=getchar();
    while(!isspace(c)){
        tab[i]=(char)c;
        i++;
        c=getchar();
    }
    tab[i]='\0';
    printf("%s\n",tab);
    free(tab);
}
