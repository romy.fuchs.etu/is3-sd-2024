#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine.h"

void init_chaine (struct chaine * A){
    A->tab=((char*)malloc(256*sizeof(char)));
    A->mbelts=0;
    A->taille=256;
    A->tab[0]='\0';
}

void ajout_en_queue_chaine(struct chaine * A,char c){
    A->tab[A->mbelts]=c;
    A->mbelts++;
    A->tab[A->mbelts]='\0';
// pour la question 3, il faudrait faire une boucle if qui vérifie la condition demandé.
}

void printf_chaine(struct chaine *A){
    printf ("%s\n", A->tab);

}

void clear_chaine(struct chaine *A){
    free(A->tab);
}
