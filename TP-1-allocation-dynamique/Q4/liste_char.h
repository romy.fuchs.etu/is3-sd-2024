#include "point.h"

struct maillon_point {
    point value;
    struct maillon_point * next;
};

struct liste_point {
    struct maillon_point * tete;
    int nbelem;
};

// ctrl R pour remplacer un mot
extern void init_liste_char (struct liste_char * );

extern void clear_liste_char (struct liste_char * C);

extern void ajouter_en_tete_liste_char (struct liste_char * , char );

extern void afficher_liste_char (struct liste_char * );

extern void ajouter_en_queue_liste_char(struct liste_char *, char );
