#include "liste_char.h"

struct chaine{
    struct liste_char L;
};

extern void init_chaine(struct chaine* L);

extern void ajout_en_queue_chaine(struct chaine * L,char c);

extern void printf_chaine(struct chaine * L);

extern void clear_chaine(struct chaine * L);
