#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "liste_char.h"
#include <assert.h>
 void init_liste_char (struct liste_char * C){
    C->tete=(struct maillon_char *) 0;
    C->nbelem=0;
}

 void clear_liste_char (struct liste_char * C){
    struct maillon_char *courant;
    struct maillon_char * suivant;
    int i;
    courant=C->tete;
    for(i=0;i<C->nbelem; i++){
        suivant=courant->next;
        free(courant);
        courant=suivant;
    }
}

void ajouter_en_tete_liste_char (struct liste_char * C, char d)
{
    struct maillon_char *nouveau;
    nouveau=(struct maillon_char *)malloc(sizeof(struct maillon_char));
    assert(nouveau !=(struct maillon_char *)0);
    nouveau->value=d;
    nouveau->next=C->tete;
    C->tete=nouveau;
    C->nbelem +=1;
}

void ajouter_en_queue_liste_char(struct liste_char *C, char d){
    struct maillon_char *M, *N;
    int i;
    if(C->nbelem==0){
        ajouter_en_tete_liste_char(C,d);
    }
    else{
    N=(struct maillon_char *)malloc(sizeof(struct maillon_char));
    N->value=d;
    M=C->tete;
    for(i=0;i<C->nbelem-1;i++){
        M=M->next;
    }
    M->next=N;
    C->nbelem+=1;
    }
}








void afficher_liste_char (struct liste_char * C  )
{
    struct maillon_char *L;
    int i;

    // printf("[");
    L=C->tete;
    for(i=0;i<C->nbelem;i++)
    {
        if(i==0)
            printf("%c",L->value);
        else
            printf("%c",L->value);
        L=L->next;
    }
    printf("\n");
}

















