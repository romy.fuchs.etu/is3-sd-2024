#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine1.h"
int main ()
{
    struct chaine S;
    int c;
    init_chaine (&S);
    c = getchar ();
    while (!isspace (c))
    {
        ajout_en_queue_chaine (&S, (char) c);
        c = getchar ();
}
    printf_chaine (&S);
    clear_chaine (&S);
    return 0;
}
