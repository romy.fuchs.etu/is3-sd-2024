#include "point.h"

struct maillon_point {
   struct point value;
    struct maillon_point * next;
};

struct liste_point {
    struct maillon_point * tete;
    int nbelem;
};

// ctrl R pour remplacer un mot
extern void init_liste_point (struct liste_point * );

extern void clear_liste_point (struct liste_point * );

extern void ajouter_en_tete_liste_point (struct liste_point * ,struct point );

extern void extraire_en_tete_liste_point(struct point *, struct liste_point *);
