#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "liste_point.h"
#include "point.h"
#include <assert.h>
 void init_liste_point (struct liste_point * C){
    C->tete=(struct maillon_point *) 0;
    C->nbelem=0;
}

 void clear_liste_point (struct liste_point * C){
    struct maillon_point *courant;
    struct maillon_point * suivant;
    int i;
    courant=C->tete;
    for(i=0;i<C->nbelem; i++){
        suivant=courant->next;
        free(courant);
        courant=suivant;
    }
}

void ajouter_en_tete_liste_point (struct liste_point * C, struct point d)
{
    struct maillon_point *nouveau;
    nouveau=(struct maillon_point *)malloc(sizeof(struct maillon_point));
    assert(nouveau !=(struct maillon_point *)0);
    nouveau->value=d;
    nouveau->next=C->tete;
    C->tete=nouveau;
    C->nbelem +=1;
}


void extraire_tete_liste_point(struct point *d, struct liste_point *C){
 struct maillon_point *tete;

    assert (C->nbelem != 0);
    tete = C->tete;
    *d = tete->value;           /* affectation */
    C->tete = tete->next;
    C->nbelem -= 1;
    free (tete);
}














