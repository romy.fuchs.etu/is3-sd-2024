#include <stdio.h>
#include "point.h"

void init_point (struct point *p, double x0, double y0, char ident0){
    p->x=x0;
    p->y=y0;
    p->ident = ident0;

}


int compare_points (const void *p0, const void *q0){
    struct point *p;
    struct point *q;
    p=(struct point *)p0;
    q=(struct point *)q0;
    double det= p->x*q->y-p->y*q->x;
    if(det>0)
        return -1;
    else
        return 1;

}


bool tourne_a_gauche (struct point *A, struct point *B, struct point *C){
    double xu,yu,xv,yv,det;
    xu=B->x-A->x;
    xv=C->x-A->x;
    yu=B->y-A->y;
    yv=C->y-A->y;
    det=xu*yv-xv*yu;
    if(det>0)
        return true;
    else
        return false;
}
