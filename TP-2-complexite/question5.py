#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 15:15:52 2024

@author: rfuchs
"""

import numpy as np
import matplotlib.pyplot as plt
K= np.loadtxt('Karatsuba.stats')
E=np.loadtxt('Elementaire.stats')
plt.plot (E[:,0],E[:,1],label="Elementaire")
plt.plot (K[:,0],K[:,1],label="Karatsuba")
plt.legend()
plt.show()

def fsimplifie(n):
    return ((17/2)*(n**log (3)/log(2)))-8*n+(3/2)
