#include <stdio.h>
#include "hachage_simple.h"
#include <stdlib.h>


void
init_table (struct table *T)
{
  int i;
  for (i = 0; i < N; i++)
    {
      init_liste_double (&(T->tab[i].L));
    }

}

void
clear_table (struct table *T)
{
  int i;
  for (i = 0; i < N; i++)
    {
      clear_liste_double (&(T->tab[i].L));
    }
}


void
enregistrer_table (struct table *T, double d)
{
  int i = (int) d % N;		//on met (int) sinon ça marche pas
  ajouter_en_tete_liste_double ((&(T->tab[i].L)), d);
}


/*bool rechercher_table(struct table * T,double d){
    bool
}*/

void
imprimer_table (struct table *T)
{
  int i;
  for (i = 0; i < N; i++)
    {
      imprimer_liste_double (&(T->tab[i].L));
    }
}

bool
rechercher_table (struct table *T, double d)
{
  int i = (int) d % N;
  return (rechercher_liste_double (&(T->tab[i].L), d));
}
