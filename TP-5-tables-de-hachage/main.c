#include <stdio.h>
#include <stdlib.h>

#include "hachage_simple.h"
int
main ()
{
  struct table T;
  double x;
  init_table (&T);
  scanf ("%lf", &x);
  while (x != -1)
    {
      enregistrer_table (&T, x);
      imprimer_table (&T);
      scanf ("%lf", &x);
    }
  scanf ("%lf", &x);
  while (x != -1)
    {
      if (rechercher_table (&T, x))
	  printf ("%lf est present dans la table de hachage\n", x);
      else
	printf ("%lf est absent de la table de hachage\n", x);
      scanf ("%lf", &x); //on met le scanf ici et pas dans if et else, sinon ça marche pass
    }

  clear_table (&T);
  return 0;
}
