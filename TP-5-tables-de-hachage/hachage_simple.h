#include "liste_double.h"
#include <stdbool.h>
#include <stdlib.h>

#define N 10

struct alveole
{
  struct liste_double L;
};

struct table
{
  struct alveole tab[N];
};


extern void init_table (struct table *);

extern void clear_table (struct table *);

extern void enregistrer_table (struct table *, double);

extern bool rechercher_table (struct table *, double);

extern void imprimer_table (struct table *);
