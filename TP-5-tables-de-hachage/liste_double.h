#if ! defined (LISTE_DOUBLE_H)
#define LISTE_DOUBLE_H 1
#include <stdbool.h>
#include <stdlib.h>


struct maillon_double
{
  double value;
  struct maillon_double *next;
};

struct liste_double
{
  struct maillon_double *tete;
  int nbelem;
};



extern void init_liste_double (struct liste_double *);

/*
 * Destructeur
 */

extern void clear_liste_double (struct liste_double *);

/*
 * Affecte une copie de src à dst
 */

extern void set_liste_double
  (struct liste_double *dst, struct liste_double *src);

/*
 * Ajout d'un double en tête de liste
 */

extern void ajouter_en_tete_liste_double (struct liste_double *, double);

/*
 * Affecte à *d la valeur du premier élément de L et supprime cet élément de L.
 * La liste L est supposée non vide.
 */

extern void extraire_tete_liste_double (double *d, struct liste_double *L);

/*
 * Impression.
 */

extern void imprimer_liste_double (struct liste_double *);
#endif

extern bool rechercher_liste_double (struct liste_double *, double);
