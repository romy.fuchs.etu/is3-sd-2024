/*
 * color_printf.h
 */

/*
 * Codes couleur
 */

#define RESET "\033[0m\033[m"
#define BLUE "\033[0;34m"
#define BOLDBLUE "\033[1;34m"
#define CYAN "\033[0;36m"
#define BOLDCYAN "\033[1;36m"
#define PURPLE "\033[0;35m"
#define BOLDPURPLE "\033[1;35m"

/* Ajoutés ! */
#define GREEN "\033[0;32m"
#define BOLDGREEN "\033[1;32m"


struct coloring_rule
{
  char *pattern;    // le motif à colorier
  char *color;      // la couleur
};

