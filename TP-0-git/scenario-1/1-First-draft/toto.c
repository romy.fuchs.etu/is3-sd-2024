#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>

/*
 * Codes couleur
 */

#define RESET "\033[0m\033[m"
#define BLUE "\033[0;34m"
#define BOLDBLUE "\033[1;34m"
#define CYAN "\033[0;36m"
#define BOLDCYAN "\033[1;36m"
#define PURPLE "\033[0;35m"
#define BOLDPURPLE "\033[1;35m"

struct coloring_rule
{
  char *pattern;    // le motif à colorier
  char *color;      // la couleur
};

/*
 * Affiche du texte sur la sortie standard mais avec de la couleur.
 * Les paramètres sont exactement les mêmes que ceux de printf mais 
 * avec un paramètre supplémentaire contenant les règles de coloriage.
 * Le paramètre supplémentaire est un tableau de 'struct coloring_rule'
 * terminé par un 'struct coloring_rule' de la forme { (char*)0, (char*)0 }.
 */

int color_printf (char *format, ...)
{
  int ret, i;
  va_list arglist;
  char *f;
  struct coloring_rule *T;
/*
 * Extraction du tableau des struct coloring_rule dans les '...'
 */
  va_start (arglist, format);
  i = 0;
  while (format[i] != '\0')
    {
      if (format[i] == '%')
        if (format[i + 1] != '%')
          {
            va_arg (arglist, void *);
            i += 1;
          }
        else
          i += 2;
      else
        i += 1;
    }
/*
 * Le tableau est affecté à la variable locale T
 */
  T = va_arg (arglist, struct coloring_rule *);
  va_end (arglist);
/*
 * Calcul du nombre de char nécessaires pour le nouveau format
 */
  int counter = 0;
  i = 0;
  while (format[i] != '\0')
    {
      int j = 0;
      bool found = false;
      while (!found && T[j].pattern != (char *) 0)
        {
          if (strncmp
              (T[j].pattern, format + i,
               strlen (T[j].pattern)) == 0)
            found = true;
          else
            j += 1;
        }
      if (found)
        {
          counter += strlen (T[j].color) +
            strlen (T[j].pattern) + strlen (RESET);
          i += strlen (T[j].pattern);
        }
      else
        {
          counter += 1;
          i += 1;
        }
    }
/*
 * Construction du nouveau format f en insérant des codes couleurs
 * à l'intérieur de l'ancien.
 */
  f = (char *) malloc ((counter + 1) * sizeof (char));
  f[0] = '\0';
  i = 0;
  while (format[i] != '\0')
    {
      int j = 0;
      bool found = false;
      while (!found && T[j].pattern != (char *) 0)
        {
          if (strncmp
              (T[j].pattern, format + i,
               strlen (T[j].pattern)) == 0)
            found = true;
          else
            j += 1;
        }
      if (found)
        {
          strcat (f, T[j].color);
          strcat (f, T[j].pattern);
          strcat (f, RESET);
          i += strlen (T[j].pattern);
        }
      else
        {
          strncat (f, format + i, 1);
          i += 1;
        }
    }
/*
 * Appel à la fonction vprintf (celle que printf aurait normalement appelée)
 */
  va_start (arglist, format);
  ret = vprintf (f, arglist);
  va_end (arglist);
/*
 * Libération du nouveau format et retour
 */
  free (f);
  return ret;
}

int main ()
{
  struct coloring_rule T[] =
    { {"%s", BOLDPURPLE}, {"%d", BOLDCYAN}, {0, 0} };

  color_printf ("\nmessage en vert: bonne année %d à %s le monde\n", 2024, "tout", T);
  return 0;
}
