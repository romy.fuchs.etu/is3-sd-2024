#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>

#include "color_printf.h"

/*
 * Retourne l'adresse du tableau des struct coloring_rules de la liste arglist
 */

struct coloring_rule * get_T (char *format, va_list arglist)
{ 
  struct coloring_rule * T;
  int i = 0;
  while (format[i] != '\0')
    {
      if (format[i] == '%')
        if (format[i + 1] != '%')
          {
            va_arg (arglist, void *);
            i += 1;
          }
        else
          i += 2;
      else
        i += 1;
    }
  T = va_arg (arglist, struct coloring_rule *);
  return T;
}

/*
 * Retourne le nouveau format obtenu en insérant des codes couleur dans format. 
 * Le nouveau format est une chaîne de caractères allouée dynamiquement
 */

char * new_format (char *format, struct coloring_rule *T)
{
  int i, f_len, reset_len;
  char *f;

  reset_len = strlen (RESET);

  f_len = 0;
  i = 0;
  while (format[i] != '\0')
    {
      int len, j = 0;
      bool found = false;
      while (!found && T[j].pattern != (char *) 0)
        {
          len  = strlen (T[j].pattern);
          if (strncmp (T[j].pattern, format + i, len) == 0)
            found = true;
          else
            j += 1;
        }
      if (found)
        {
          f_len += strlen (T[j].color) + len + reset_len;
          i += len;
        }
      else
        {
          f_len += 1;
          i += 1;
        }
    }

  f = (char *) malloc ((f_len + 1) * sizeof (char));
  f[0] = '\0';

  i = 0;
  while (format[i] != '\0')
    {
      int len, j = 0;
      bool found = false;
      while (!found && T[j].pattern != (char *) 0)
        {
          len = strlen (T[j].pattern);
          if (strncmp (T[j].pattern, format + i, len) == 0)
            found = true;
          else
            j += 1;
        }
      if (found)
        {
          strcat (f, T[j].color);
          strcat (f, T[j].pattern);
          strcat (f, RESET);
          i += len;
        }
      else
        {
          strncat (f, format + i, 1);
          i += 1;
        }
    }

    return f;
}

/*
 * Affiche du texte sur la sortie standard mais avec de la couleur.
 * Les paramètres sont exactement les mêmes que ceux de printf mais 
 * avec un paramètre supplémentaire contenant les règles de coloriage.
 * Le paramètre supplémentaire est un tableau de 'struct coloring_rule'
 * terminé par un 'struct coloring_rule' de la forme { (char*)0, (char*)0 }.
 */

int color_printf (char *format, ...)
{
  int ret, i;
  va_list arglist;
  struct coloring_rule *T;
  char * f;

  va_start (arglist, format);
  T = get_T (format, arglist);
  va_end (arglist);

  f = new_format (format, T);

  va_start (arglist, format);
  ret = vprintf (f, arglist);
  va_end (arglist);

  free (f);
  return ret;
}

/*
 * Main modifié
 */

int main ()
{
  struct coloring_rule T[] =
    { {"message en vert:", BOLDGREEN }, {"%s", BOLDPURPLE}, {"%d", BOLDCYAN}, {0, 0} };

  color_printf ("\nmessage en vert: bonne année %d à %s le monde\n", 2024, "tout", T);
  return 0;
}
