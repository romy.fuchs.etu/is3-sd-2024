Ce répertoire contient 
1. le module de liste de doubles du support de cours
2. un programme principal (main.c) qui permet de le tester
3. un Makefile.

Pour obtenir l'exécutable (main) :
make

Pour exécuter l'exécutable :
./main
[3.140000, 0.000000, -0.100000]

Pour supprimer les fichiers inutiles après exécution :
make clean

Attention à ne pas modifier les fichiers présents dans ce répertoire
sous peine de créer des conflits. Si vous voulez les modifier, faites
des copies et modifiez les copies
